#include <QApplication>

#include <QMainWindow>
#include <QSplashScreen>
#include <QLabel>

#include "mainwindow.hpp"
#include <QDebug>


Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
/*
#ifdef Q_OS_ANDROID_FAKE
    QDir d(__FILE__); d.cdUp(); d.cdUp(); d.cdUp();
    QIcon::setThemeSearchPaths({d.filePath("data/icons/breeze-icons/")});
#else
    QIcon::setThemeSearchPaths({"assets:/icons"});
#endif
    QIcon::setThemeName("icons");
*/
    MainWindow window;
    window.show();

    int ret = app.exec();

    return ret;
}

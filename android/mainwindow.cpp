#include "mainwindow.hpp"

#include <QApplication>
#include <QCoreApplication>
#include <QResizeEvent>
#include <QtVersion>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QMessageBox>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)

{
    QWidget* central = new QWidget(this);
    QVBoxLayout* vbox = new QVBoxLayout(central);
    central->setLayout(vbox);
    setCentralWidget(central);


    QLabel* label = new QLabel(QStringLiteral("Hello Qt %1 for Android!").arg(QLatin1String(qVersion())), this);
    label->setAlignment(Qt::AlignCenter);
    label->setStyleSheet(QStringLiteral("background-color:#006600; color:#FFFFFF"));

    QPushButton* buttonOne = new QPushButton(QStringLiteral("Show a message"), this);

    connect(buttonOne, &QPushButton::clicked, this, [&](bool) {
        QMessageBox::aboutQt(this);
    });

    QPushButton* buttonTwo = new QPushButton(QStringLiteral("Open a file"), this);

    connect(buttonTwo, &QPushButton::clicked, this, [&](bool) {
        QString filename = QFileDialog::getOpenFileName(this, QStringLiteral("Open File"),
                                                QString(),
                                                QStringLiteral("Images (*.png *.xpm *.jpg)"));
        QMessageBox::information(this, QStringLiteral("Choosing done"), filename);
    });

    vbox->addWidget(label);
    vbox->addWidget(buttonOne);
    vbox->addWidget(buttonTwo);
}

MainWindow::~MainWindow() {

}

//void MainWindow::resizeEvent(QResizeEvent /*event*/) {
/*    m_pLabel->setGeometry(QApplication::desktop()->screenGeometry());
}*/
